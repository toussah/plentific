import json

from datetime import datetime
from django.http import HttpResponse
from models import PropertyTransaction
from utils import get_brackets


def get_average_price_by_range(request):
    """
    Returns all the prices of property transaction in a given date range area.
    It returns a dictionary where each key is a type of property (S, F, T or D).
    The values are (date, price) tuples ordered by date.

    :param request: The request sent by the front-end should hold the date range and partial post code.
    :return An HTTP response with the dates and prices per type of property.
    """
    # Dates are allegedly validated from the front end, we could validate the area here.
    area = request.POST.get("area")
    date_from = request.POST.get("date_from")
    date_to = request.POST.get("date_to")

    average_prices = {}
    transactions_in_range = PropertyTransaction.objects.filter(
        transaction_date__range=[date_from, date_to],
        post_code__startswith=area
    ).order_by('transaction_date')

    for property_type in PropertyTransaction.PropertyType.attributes:
        average_prices[property_type] = [
            (t.transaction_date.strftime("%Y-%m-%d"), t.price) for t in transactions_in_range.filter(
                property_type=property_type
            )
        ]
    return HttpResponse(json.dumps({"average_prices": average_prices}), content_type="application/json")


def get_transaction_bracket(request):
    """
    Returns the number of transaction in a given date for 8 prices of bracket.
    :param request: The request should give the bracket range and date to filter by.
    :return An HTTP Response with the number of transaction per bracket.
    """
    # It is assumed here that the date and the bracket is valid from front-end validation.
    # The bracket is derived from the location.
    bracket = int(request.POST.get("bracket"))
    date_filter = datetime.strptime(request.POST.get("date_filter"), "%Y-%m")

    transactions_by_date = PropertyTransaction.objects.filter(
        transaction_date__year=date_filter.year,
        transaction_date__month=date_filter.month
    )

    transaction_list = []

    bracket_ranges = get_brackets(bracket, 8)
    for bracket_range in bracket_ranges:
        price_min, price_max = bracket_range
        if price_max < 0:
            transaction_bracket = transactions_by_date.filter(price__gte=price_min)
        else:
            transaction_bracket = transactions_by_date.filter(price__gte=price_min, price__lt=price_max)
        transaction_list.append(transaction_bracket.count())

    return HttpResponse(json.dumps({"transactions": transaction_list}), content_type="application/json")

from django.db import migrations
from housing.utils import download_transaction


def populate_transaction_property(apps, schema_editor):
    if schema_editor.connection.alias != 'default':
        return
    transaction_model = apps.get_model('housing', 'PropertyTransaction')
    for year in xrange(2011, 2017):
        for transaction_kwargs in download_transaction(year):
            transaction_model.objects.create(**transaction_kwargs)


class Migration(migrations.Migration):

    dependencies = [
        ('housing', '0001_initial')
    ]

    operations = [
        migrations.RunPython(populate_transaction_property),
    ]

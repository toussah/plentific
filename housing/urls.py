from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^average_price$', views.get_average_price_by_range, name='average_price'),
    url(r'^transaction_bracket$', views.get_transaction_bracket, name='transaction_bracket'),
]

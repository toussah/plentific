# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import Avg
from djchoices import DjangoChoices, ChoiceItem

# Create your models here.


class PropertyTransaction(models.Model):
    """This table stores the transactions of bought properties including the price, date and post code."""

    class PropertyType(DjangoChoices):
        flat = ChoiceItem("F")
        terraced = ChoiceItem("T")
        detached = ChoiceItem("D")
        semi_detached = ChoiceItem("S")

    # When the transaction took place.
    transaction_date = models.DateTimeField()
    # The property's post code.
    post_code = models.CharField(max_length=10)
    # The type of property (see PropertyType class)
    property_type = models.CharField(max_length=1, choices=PropertyType.choices)
    # The price of the property for that transaction.
    price = models.IntegerField()


# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.test import TestCase, RequestFactory
from models import PropertyTransaction
from utils import dict_to_transaction, get_brackets
from views import get_average_price_by_range, get_transaction_bracket

import json
# Create your tests here.


class PropertyTransactionTests(TestCase):
    """Test PropertyTransaction table features."""

    def setUp(self):
        self.factory = RequestFactory()
        dates = [
            "2011-03-02 00:00",
            "2012-04-02 00:00",
            "2013-03-07 00:00",
            "2014-10-12 00:00",
            "2015-07-20 00:00",
        ]
        prices = ["10", "14", "15", "25", "30", "40", "100"]
        post_codes = ["N1 8PA", "WC1V 7HE", "N1 7PP"]

        for date in dates:
            for price in prices:
                for post_code in post_codes:
                    for property_type in ("D", "F"):
                        PropertyTransaction.objects.create(
                            **dict_to_transaction(
                                {
                                    "price": price,
                                    "post_code": post_code,
                                    "property_type": property_type,
                                    "transaction_date": date
                                }
                            )
                        )

    def tearDown(self):
        PropertyTransaction.objects.all().delete()

    def test_get_brackets(self):
        bracket = get_brackets(100, 4)
        self.assertEqual(bracket, [(0, 100), (100, 200), (200, 300), (300, -1)])

    def test_get_average_price_by_range(self):
        url = reverse("average_price")
        request = self.factory.post(url, data={
            "area": "WC1V",
            "date_from": "2011-03-02",
            "date_to": "2013-03-07"
        })
        average_prices = json.loads(get_average_price_by_range(request).content)["average_prices"]
        self.assertEqual(len(average_prices["S"]), 0)
        self.assertEqual(len(average_prices["T"]), 0)
        self.assertEqual(len(average_prices["D"]), 21)
        self.assertEqual(len(average_prices["F"]), 21)

        request = self.factory.post(url, data={
            "area": "N1",
            "date_from": "2011-03-02",
            "date_to": "2013-03-07"
        })
        average_prices = json.loads(get_average_price_by_range(request).content)["average_prices"]
        self.assertEqual(len(average_prices["S"]), 0)
        self.assertEqual(len(average_prices["T"]), 0)
        self.assertEqual(len(average_prices["D"]), 42)
        self.assertEqual(len(average_prices["F"]), 42)

        request = self.factory.post(url, data={
            "area": "N2",
            "date_from": "2012-04-01",
            "date_to": "2013-03-07"
        })
        average_prices = json.loads(get_average_price_by_range(request).content)["average_prices"]
        self.assertEqual(len(average_prices["S"]), 0)
        self.assertEqual(len(average_prices["T"]), 0)
        self.assertEqual(len(average_prices["D"]), 0)
        self.assertEqual(len(average_prices["F"]), 0)

        request = self.factory.post(url, data={
            "area": "N1",
            "date_from": "2012-04-01",
            "date_to": "2015-07-21"
        })
        average_prices = json.loads(get_average_price_by_range(request).content)["average_prices"]
        self.assertEqual(len(average_prices["S"]), 0)
        self.assertEqual(len(average_prices["T"]), 0)
        self.assertEqual(len(average_prices["D"]), 56)
        self.assertEqual(len(average_prices["F"]), 56)

    def test_get_transaction_bracket(self):
        url = reverse("transaction_bracket")
        request = self.factory.post(url, data={
            "bracket": 5,
            "date_filter": "2011-03"
        })
        transaction_bracket = json.loads(get_transaction_bracket(request).content)["transactions"]
        self.assertEqual(transaction_bracket, [0, 0, 12, 6, 0, 6, 6, 12])

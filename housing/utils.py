import csv
import pytz
import re
import requests

from django.utils.dateparse import parse_datetime
from housing.models import PropertyTransaction

CSV_URL = "http://prod1.publicdata.landregistry.gov.uk.s3-website-eu-west-1.amazonaws.com/pp-{year}.txt"


def _valid_transaction(transaction_dict):
    # This could raise some errors if the database ever changes, they should be handled.
    return transaction_dict["price"].isdigit() \
        and bool(parse_datetime(transaction_dict["transaction_date"])) \
        and transaction_dict["property_type"] in PropertyTransaction.PropertyType.attributes \
        and bool(re.match(r'[A-Z][A-Z0-9]{1,3} \d[A-Z]{2}$', transaction_dict["post_code"]))


def _row_to_dict(row):
    fields = [
        "id",
        "price",
        "transaction_date",
        "post_code",
        "property_type",
    ]
    transaction_dict = {field: row[index] for index, field in enumerate(fields)}
    if _valid_transaction(transaction_dict):
        return transaction_dict


def dict_to_transaction(transaction):
    """Convert a dictionary into a PropertyTransaction object."""
    price = int(transaction["price"])

    # This should not be hardcoded and we should handle different territories.
    london_tz = pytz.timezone('Europe/London')
    naive_transaction_date = parse_datetime(transaction["transaction_date"])
    transaction_date = london_tz.localize(naive_transaction_date)
    post_code = transaction["post_code"]
    property_type = transaction["property_type"]
    return {
        "price": price,
        "transaction_date": transaction_date,
        "property_type": property_type,
        "post_code": post_code
    }


def download_transaction(year):
    """Download all the transactions of properties made into a certain year and yields transactions objects."""

    with requests.Session() as s:
        download = s.get(CSV_URL.format(year=year))

        reader = csv.reader(download.content.splitlines(), delimiter=",")
        for row in reader:
            transaction_dict = _row_to_dict(row)
            if transaction_dict:
                yield dict_to_transaction(transaction_dict)


def get_brackets(bracket_range, number_of_brackets):
    """
    Returns Ranges depending of a bracket range
    :param bracket_range: The size of each bracket
    :param number_of_brackets: The number of brackets
    :return: Some bracket ranges
    """
    brackets = []
    for i in xrange(number_of_brackets - 1):
        brackets.append((i * bracket_range, (i+1) * bracket_range))
    brackets.append(((number_of_brackets - 1) * bracket_range, -1))
    return brackets
